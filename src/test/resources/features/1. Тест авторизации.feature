#language: ru
Функционал: Вход в систему
  Сценарий: Успешная авторизация пользователя
    Дано пользователь заходит на главную страницу
    Когда вводит логин "user@pflb.ru"
    И вводит пароль "user"
    И нажимает кнопку "Go"
    Тогда появляется окно со словом "Successful"
    Тогда нажать кнопку согласия в появившемся окне