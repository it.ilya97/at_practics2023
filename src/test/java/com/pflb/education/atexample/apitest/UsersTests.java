package com.pflb.education.atexample.apitest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pflb.education.atexample.apitest.API.CarApiMethods;
import com.pflb.education.atexample.apitest.API.UserApiMethods;
import com.pflb.education.atexample.pojo.Car;
import com.pflb.education.atexample.pojo.User;
import io.qameta.allure.Description;
import io.qameta.allure.Step;
import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static com.pflb.education.atexample.apitest.API.CarApiMethods.createCar;
import static com.pflb.education.atexample.apitest.API.CarApiMethods.deleteCarById;
import static com.pflb.education.atexample.apitest.API.UserApiMethods.*;
import static io.restassured.RestAssured.delete;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.notNullValue;

public class UsersTests extends BaseApiTest{

    @Test
    @Description("Получение списка пользователей из JSON Body и вывод в консоль))")
    public void responseUsersListTest() {
        List<User> responseBody;

        Response getUsersResponse = getUsersList();

        responseBody = getUsersResponse
                .then()
                .extract()
                .body()
                .as(new TypeRef<List<User>>() {});

        List<Integer> ids = responseBody.stream()
                .map(User::getId).toList();
        ids.forEach(System.out::println);
    }

    @Test
    @Description("""
            1. Создать пользователя методом POST,  проверить на статус ответа и не пустое тело,
            2. Получить созданного пользователя методом GET по id, проверить на статус ответа и не пустое тело
            3. Сравнить пользователей из тела запроса и ответа на равенство
            4. Удалить созданного пользователя по id, проверить на статус ответа и не пустое тело""")
    public void createUserWithValidDataGetHimAndDelete_Test() throws IOException {

        InputStream testData = BaseApiTest.class.getClassLoader().getResourceAsStream("usersTestData1.json");
        User requestUser = new ObjectMapper().readValue(testData, User.class);

        Response response = createUser(requestUser);

        response.then()
                .statusCode(201)
                .body(notNullValue());

        requestUser.id = response.path("id");

        Response getResponse =  getUserById(requestUser.id);

        getResponse.then()
                .statusCode(200)
                .body(notNullValue());

        User userFromGetResponse = getResponse.getBody().as(User.class);

        Assertions.assertEquals(requestUser, userFromGetResponse);

        Response deleteResponse = deleteUserById(userFromGetResponse.id);

        deleteResponse.then()
                .statusCode(204)
                .body(notNullValue());
    }

    @Test
    @Description("Тест на создание пользователя с невалидными данными(money - строкой, в ответе код 400 Bad Request")
    public void newUserNeg_Test() throws IOException {

        InputStream testData = BaseApiTest.class.getClassLoader().getResourceAsStream("usersTestData2.json");

        Response createUser = createUser(testData);

        createUser.then()
                .statusCode(400);
    }

    @Test
    @Description(""" 
            Проверить возможность покупки пользователем автомобиля
            1. Создать пользователя с money 15000;
            2. Создать автомобиль со стоимостью меньше, чем поле money у пользователя из шага 1.
            3. Вызвать метод покупки пользователем автомобиля, передав id созданных пользователя и автомобиля из шагов 1 и 2.
            4. Проверить ответ сервера на статус ответа
            5. Вызвать у пользователя метод получения списка авто и проверить наличие купленного авто на шаге 3
            6. Проверить невозможность удаления созданного пользователя и автомобиля т.к. за ними прикреплена другая сущность и сначала надо продать авто""")
    public void userBuyCar_Test() throws IOException {

        User requestUser = new User(43, "Ночной", "Жор", 15000.0, 45, User.Sex.MALE);
        Car requestCar = new ObjectMapper().readValue(BaseApiTest.class.getClassLoader().getResourceAsStream("carsTestData.json"), Car.class);

        Integer newUserId  = createUser(requestUser)
                .then()
                .statusCode(201)
                .extract()
                .path("id");

        Integer newCarId  = createCar(requestCar)
                .then()
                .statusCode(201)
                .extract()
                .path("id");

        requestUser.id = newUserId;
        requestCar.id = newCarId;

        Response userBuyCarResponse = userBuyCar(requestUser, requestCar);

        userBuyCarResponse.then()
                .statusCode(200);

        User userFromBuyRes = userBuyCarResponse.getBody().as(User.class);

        Assertions.assertNotEquals(requestUser.money, userFromBuyRes.money);

        ArrayList<Car> carsFromUser = getUserCars(requestUser)
                .then()
                .statusCode(200)
                .extract()
                .body()
                .as(new TypeRef<ArrayList<Car>>() {});

        deleteUserById(newUserId)
                .then()
                .statusCode(409);

        deleteCarById(newCarId)
                .then()
                .statusCode(409);
    }
}
