package com.pflb.education.atexample.apitest.API;

import com.pflb.education.atexample.pojo.Car;
import com.pflb.education.atexample.pojo.User;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.InputStream;

import static io.restassured.RestAssured.given;

public class CarApiMethods {
    @Step("GET-запрос. Получить список автомобилей методом GET")
    public static Response getCarsList(){

        Response response = given()
                .when()
                .get("/cars");

        return response;
    }

    @Step("POST-запрос. Создать автомобиль {car.id} {car.model} {car.mark}")
    public static Response createCar(Car car){

        return given()
                .body(car)
                .when()
                .post("/car");
    }

    @Step("POST-запрос. Создать автомобиль, отправив в тело Json")
    public static Response createCar(InputStream inputStream){

        return given()
                .body(inputStream)
                .when()
                .post("/car");
    }

    @Step("PUT-запрос. Изменить автомобиль {user.id} {user.firstName} {user.secondName}")
    public static Response editCar(Integer carId, Car car){

        return given()
                .contentType(ContentType.JSON)
                .body(car)
                .when()
                .put("/car/" + carId);
    }

    @Step("DELETE-запрос. Удалить автомобиль с id = {id} ")
    public static Response deleteCarById(Integer carId) {

        return given()
                .contentType(ContentType.JSON)
                .when()
                .delete("/car/" + carId);
    }

    @Step("GET-запрос. Получить автомобиль с id = {id} ")
    public static Response getCarById(Integer carid) {

        return given()
                .contentType(ContentType.JSON)
                .when()
                .get("/car/" + carid);
    }
}
