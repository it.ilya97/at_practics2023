package com.pflb.education.atexample.apitest.API;

import com.pflb.education.atexample.pojo.Car;
import com.pflb.education.atexample.pojo.User;
import io.qameta.allure.Step;
import io.restassured.common.mapper.TypeRef;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.InputStream;
import java.util.ArrayList;

import static io.restassured.RestAssured.given;

public class UserApiMethods {
    @Step("Создать пользователя {user.id} {user.firstName} {user.secondName}")
    public static Response createUser(User user){

        return given()
                .body(user)
                .when()
                .post("/user");
    }

    @Step("Создать пользователя, отправив в тело Json")
    public static Response createUser(InputStream inputStream){

        return given()
                .body(inputStream)
                .when()
                .post("/user");
    }

    @Step("Удалить пользователя с id = {id} ")
    public static Response deleteUserById(Integer id) {

        return given()
                .contentType(ContentType.JSON)
                .when()
                .delete("/user/" + id);
    }

    @Step("Получить пользователя с id = {id} ")
    public static Response getUserById(Integer id) {

        return given()
                .contentType(ContentType.JSON)
                .when()
                .get("/user/" + id);
    }

    @Step("Изменение пользователя с id = user.id")
    public static Response editUser(User user){
        return given()
                .body(user)
                .when()
                .post("/user");
    }

    @Step("Покупка пользователем с id = {user.id} автомобиля с id = {car.id} ")
    public static Response userBuyCar(User user, Car car){
        return given()
                .body(user)
                .when()
                .post("/user/" + user.id + "/buyCar/" + car.id);
    }

    @Step("Получить список автомобилей пользователя с id = {user.id}")
    public static Response getUserCars(User user){
        return given()
                .body(user)
                .when()
                .get("/user/" + user.id + "/cars");
    }

    @Step("Получить пользователя с id = {user.id} и его имущество(cars, houses)")
    public static Response getUserInfo(User user){
        return given()
                .body(user)
                .when()
                .get("/user/" + user.id + "/info");
    }

    @Step("Начисление пользователю с id = {user.id} денег в количестве {amount} ")
    public static Response addMoneyToUser(User user, Double amount){
        return given()
                .body(user)
                .when()
                .post("/user/" + user.id + "/money/" + amount);
    }

    @Step("Продажа пользователeм с id = {user.id} автомобиля с id = {car.id} ")
    public static Response sellCarByUser(User user, Car car){
        return given()
                .body(user)
                .when()
                .post("/user/" + user.id + "/sellCar/" + car.id);
    }
    @Step("Получить список пользователей")
    public static Response getUsersList(){

        Response response = given()
                .when()
                .get("/users");

        return response;
    }
    }