package com.pflb.education.atexample.devtools_examples;

import com.pflb.education.atexample.config.ApplicationConfig;
import com.pflb.education.atexample.page.LoginPage;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.v115.browser.Browser;
import org.openqa.selenium.devtools.v115.browser.model.Bounds;
import org.openqa.selenium.devtools.v115.browser.model.BrowserCommandId;
import org.openqa.selenium.devtools.v115.browser.model.Histogram;
import org.openqa.selenium.devtools.v115.browser.model.WindowID;
import org.openqa.selenium.devtools.v115.emulation.model.ScreenOrientation;
import org.openqa.selenium.devtools.v115.network.Network;
import org.openqa.selenium.devtools.v115.page.Page;
import org.openqa.selenium.devtools.v115.performance.Performance;
import org.openqa.selenium.devtools.v115.performance.model.Metric;
import org.openqa.selenium.devtools.v115.performance.model.Metrics;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DevToolsTests {

    /*
    Documentation:
                https://chromedevtools.github.io/devtools-protocol/
    Perfomance metrics -> what does each metrics mean:
                https://github.com/puppeteer/puppeteer/blob/main/docs/api/puppeteer.page.metrics.md
                https://web.dev/metrics/
     */
    private ChromeDriver driver;

    private WebDriverWait wait;

    private ApplicationConfig config;

    private DevTools devTools;
    @BeforeAll
    public void configInit() {
        config = new ApplicationConfig();

    }

    @BeforeEach
    public void init() throws MalformedURLException {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--start-maximized");
        chromeOptions.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(chromeOptions);
        wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        devTools = driver.getDevTools();
        devTools.createSession();
        //driver.get("https://www.yandex.com");

    }

    @Test
    public void getVersion() {
        driver.get("https://www.yandex.com");
        Browser.GetVersionResponse response = devTools.send(Browser.getVersion());

        assertNotEquals(response.getJsVersion(), "");
        assertEquals(response.getProtocolVersion(), "1.3");
        assertNotEquals(response.getRevision(), "");
        assertTrue(response.getUserAgent().contains("Chrome/"));
        assertTrue(response.getProduct().contains("Chrome/"));
    }

    @Test
    public void deviceModeOverride() throws InterruptedException {
        Map<String, Object> args = new HashMap<>();
        ScreenOrientation screenOrientation = new ScreenOrientation(ScreenOrientation.Type.LANDSCAPEPRIMARY, 50);
        args.put("width", 390);
        args.put("height", 844);
        args.put("deviceScaleFactor", 100);
        args.put("mobile", true);
        args.put("screenOrientation", screenOrientation);

        driver.executeCdpCommand("Emulation.setDeviceMetricsOverride", args);

        driver.get(config.baseUrl);

        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.fillLoginInput(config.userName);
        loginPage.fillPasswordInput(config.userPassword);
        loginPage.submitForm();
    }

    @Test
    public void getPerfMetrics_Test(){
        devTools.send(Performance.enable(Optional.empty()));
        List<Metric> perfMetrics = devTools.send(Performance.getMetrics());

        driver.get(config.baseUrl);

        for (Metric metric: perfMetrics) {
            System.out.println(metric.getName() + ":" + metric.getValue());
        }

        System.out.println("########################################################################################");
    }


}
