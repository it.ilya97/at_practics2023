package com.pflb.education.atexample;

import com.pflb.education.atexample.config.ApplicationConfig;
import com.pflb.education.atexample.pojo.User;
import com.pflb.education.atexample.page.LoginPage;
import com.pflb.education.atexample.page.NavBar;
import com.pflb.education.atexample.page.UserPage;
import io.qameta.allure.Attachment;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.v115.performance.Performance;
import org.openqa.selenium.devtools.v115.performance.model.Metric;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.time.Duration;
import java.util.*;


@TestInstance(Lifecycle.PER_CLASS)
    public class EduPlatformSeleniumTests {
    private ChromeDriver driver;

    private WebDriverWait wait;

    private ApplicationConfig config;

    private DevTools devTools;

    @BeforeAll
    public void configInit() {
        config = new ApplicationConfig();
    }

    @BeforeEach
    public void init() throws MalformedURLException {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(chromeOptions);
        wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        devTools = driver.getDevTools();
        devTools.createSession();
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void loginTest() {
        driver.get(config.baseUrl);
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.fillLoginInput(config.userName);
        loginPage.fillPasswordInput(config.userPassword);
        loginPage.submitForm();
        String alertText = loginPage.getAlertText();

        Assertions.assertTrue(alertText.contains("Successful"), "Alert text doesn't contains info about successful auth");
    }

    @Test
    public void sortUsersByID_AscendingTest() {
        devTools.send(Performance.enable(Optional.empty()));
        List<Metric> perfMetrics = devTools.send(Performance.getMetrics());

        driver.get(config.baseUrl + "/read/users");
        UserPage userPage = new UserPage(driver, wait);

        sendPerfMetricsToReport(perfMetrics);

        //явное ожидание загрузки таблицы, без него у меня падает, видимо инет плохой)
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("table tbody tr")));

        NavBar navBar = new NavBar(driver, wait);

        //одиночный клик для сортировки по возрастанию
        navBar.sortById();

        List<Integer> usersIds = new ArrayList<>(userPage.parseUsersId());

        List<Integer> userIdsCopy = new ArrayList<>(usersIds);

        Collections.sort(userIdsCopy);

        Assertions.assertEquals(userIdsCopy, usersIds, "Sorting users by Ascending Id in users table is incorrect");

    }

    @Test
    public void sortUsersByID_DescendingTest() {
        driver.get(config.baseUrl + "/read/users");
        UserPage userPage = new UserPage(driver, wait);

        //явное ожидание загрузки таблицы, без него у меня падает, видимо инет плохой)
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("table tbody tr")));

        NavBar navBar = new NavBar(driver, wait);

        //двойной клик для сортировки по убыванию
        navBar.sortById();
        navBar.sortById();

        List<Integer> usersIds = new ArrayList<>(userPage.parseUsersId());

        List<Integer> userIdsCopy = new ArrayList<>(usersIds);

        Collections.sort(userIdsCopy, Collections.reverseOrder());

        Assertions.assertEquals(userIdsCopy, usersIds, "Sorting users by Descending Id in users table is incorrect");
    }

    @Attachment(value = "Perfomance metrics:", type = "text/html")
    public String sendPerfMetricsToReport(List<Metric> perfMetrics) {

        List<String> perfMetricsStringList = new ArrayList<>();

        for (Metric metric : perfMetrics) {
            perfMetricsStringList.add(metric.getName() + "=" + metric.getValue());
        }

        return String.join("\n", perfMetricsStringList);
    }
}