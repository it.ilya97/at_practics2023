package com.pflb.education.java.homework.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pflb.education.java.homework.http.dto.Authorization;
import com.pflb.education.java.homework.http.dto.Authorization.AuthRequest;
import com.pflb.education.java.homework.http.dto.Authorization.AuthResponse;
import com.pflb.education.java.homework.http.dto.User;
import com.pflb.education.java.homework.http.dto.User.Sex;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublisher;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandler;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.charset.StandardCharsets;

public class Main {
  public static final String BASE_URL = "http://77.50.236.203:4879";
  private static ObjectMapper objectMapper = new ObjectMapper();

  public static void main(String[] args) throws IOException, InterruptedException {
    AuthRequest authRequest = new AuthRequest();
    authRequest.username = "admin@pflb.ru";
    authRequest.password = "admin";

    User user = new User();
    user.firstName = "Vasya";
    user.secondName = "Valiliev";
    user.sex = Sex.MALE;
    user.age = 26;
    user.money = 0;

    AuthResponse authResponse = authorize(authRequest);
    User createdUser = createUser(authResponse, user);
    addMoney(authResponse, createdUser, 200000);
//    String houseId = createHouse(authToken);
    settleUser(authResponse, createdUser, 4);
  }

  public static void oldHttpClient() throws IOException {
    //Authorization
    URL loginUrl = URI.create(BASE_URL).resolve("/login").toURL();
    String loginRequestBody = "{\"username\": \"admin@pflb.ru\", \"password\": \"admin\"}";
    HttpURLConnection loginConnection = (HttpURLConnection) loginUrl.openConnection();
    loginConnection.setDoOutput(true);
    loginConnection.setRequestMethod("POST");
    loginConnection.setRequestProperty("Content-Type", "application/json");
    OutputStream loginOutputStream = loginConnection.getOutputStream();
    loginOutputStream.write(loginRequestBody.getBytes(StandardCharsets.UTF_8));
    int loginStatusCode = loginConnection.getResponseCode();
    InputStream loginConnectionInputStream = loginStatusCode >= 200 && loginStatusCode < 300
        ? loginConnection.getInputStream()
        : loginConnection.getErrorStream();
//    InputStream loginConnectionInputStream = loginConnection.getInputStream();
    byte[] loginResponseBytes = loginConnectionInputStream.readAllBytes();
    String loginResponse = new String(loginResponseBytes, StandardCharsets.UTF_8);
    String[] loginResponseParts = loginResponse.split("\"");
    String token = loginResponseParts[3];
    System.out.println(loginResponse);

    // Create user
    String requestBody = """
        {
          "firstName": "Василий",
          "age": 19,
          "money": 0,
          "sex": "MALE",
          "secondName": "Тестов2"
        }""";
    URL url = URI.create(BASE_URL).resolve("/user").toURL();
    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
    connection.setRequestMethod("POST");
    connection.setDoOutput(true);
    connection.setRequestProperty("Content-Type", "application/json");
    connection.setRequestProperty("Authorization", "Bearer " + token);
    OutputStream outputStream = connection.getOutputStream();
    outputStream.write(requestBody.getBytes(StandardCharsets.UTF_8));

    int responseCode = connection.getResponseCode();
    InputStream inputStream = responseCode >= 200 && responseCode < 300
        ? connection.getInputStream()
        : connection.getErrorStream();
    byte[] responseBytes = inputStream.readAllBytes();
    String response = new String(responseBytes, StandardCharsets.UTF_8);
    String userId = response.split(":")[1].split(",")[0];
    System.out.printf("Response code: %d\nBody: %s", responseCode, response);

    //Add money
    String strUri = String.format("/user/%s/30", userId);
    URL addMoneyUrl = URI.create(BASE_URL).resolve(strUri).toURL();
    HttpURLConnection addMoneyConnection = (HttpURLConnection) addMoneyUrl.openConnection();
    addMoneyConnection.setRequestMethod("POST");
    addMoneyConnection.setDoOutput(true);
//    connection.setRequestProperty("Content-Type", "application/json");
    addMoneyConnection.setRequestProperty("Authorization", "Bearer " + token);

    InputStream addMoneyConnectionInputStream = addMoneyConnection.getInputStream();
    byte[] addMoneyResponseBytes = addMoneyConnectionInputStream.readAllBytes();
    int addMoneyResponseCode = addMoneyConnection.getResponseCode();
    String addMoneyResponse = new String(addMoneyResponseBytes, StandardCharsets.UTF_8);
    System.out.printf("Response code: %d\nBody: %s", addMoneyResponseCode, addMoneyResponse);
  }

  public static Authorization.AuthResponse authorize(Authorization.AuthRequest authRequest) throws IOException, InterruptedException {
    URI uri = URI.create(BASE_URL).resolve("/login");
//    String requestBody = "{\"username\": \"user@pflb.ru\", \"password\": \"user\"}";
    String requestBody = objectMapper.writeValueAsString(authRequest);
    BodyPublisher bodyPublisher = BodyPublishers.ofString(requestBody, StandardCharsets.UTF_8);
    BodyHandler<String> bodyHandler = BodyHandlers.ofString(StandardCharsets.UTF_8);
    HttpRequest request = HttpRequest.newBuilder()
        .uri(uri)
        .header("Content-Type", "application/json")
        .POST(bodyPublisher)
        .build();
    HttpClient client = HttpClient.newHttpClient();
    HttpResponse<String> response = client.send(request, bodyHandler);
    System.out.printf("Response code: %d\nBody: %s\n", response.statusCode(), response.body());
//    String[] responseParts = response.body().split("\"");
//    String token = responseParts[3];
//    return token;
    AuthResponse authResponse = objectMapper.readValue(response.body(), AuthResponse.class);
    return authResponse;
  }

  public static User createUser(Authorization.AuthResponse authorization, User user) throws IOException, InterruptedException {
    URI uri = URI.create(BASE_URL).resolve("/user");
//    String requestBody = """
//        {
//          "firstName": "Василий",
//          "age": 19,
//          "money": 0,
//          "sex": "MALE",
//          "secondName": "Тестов2"
//        }""";

    String requestBody = objectMapper.writeValueAsString(user);
    BodyPublisher bodyPublisher = BodyPublishers.ofString(requestBody, StandardCharsets.UTF_8);
    BodyHandler<String> bodyHandler = BodyHandlers.ofString(StandardCharsets.UTF_8);
    HttpRequest request = HttpRequest.newBuilder()
        .uri(uri)
        .header("Content-Type", "application/json")
        .header("Authorization", "Bearer " + authorization.accessToken)
        .POST(bodyPublisher)
        .build();
    HttpClient client = HttpClient.newHttpClient();
    HttpResponse<String> response = client.send(request, bodyHandler);
    System.out.printf("Response code: %d\nBody: %s\n", response.statusCode(), response.body());
//    String id = response.body().split(":")[1].split(",")[0];
//    return id;
    User userResp = objectMapper.readValue(response.body(), User.class);
    return userResp;
  }

  public static void addMoney(Authorization.AuthResponse authorization, User user, double amount)
      throws IOException, InterruptedException {
    BodyPublisher bodyPublisher = BodyPublishers.noBody();
    String strUri = String.format("/user/%d/money/%.2f", user.id, amount);
    URI uri = URI.create(BASE_URL).resolve(strUri);
    BodyHandler<String> bodyHandler = BodyHandlers.ofString(StandardCharsets.UTF_8);
    HttpRequest request = HttpRequest.newBuilder()
        .uri(uri)
        .header("Content-Type", "application/json")
        .header("Authorization", "Bearer " + authorization.accessToken)
        .POST(bodyPublisher)
        .build();
    HttpClient client = HttpClient.newHttpClient();
    HttpResponse<String> response = client.send(request, bodyHandler);
    System.out.printf("Response code: %d\nBody: %s\n", response.statusCode(), response.body());

  }

  public static String createHouse(String authToken) throws IOException, InterruptedException {
    URI uri = URI.create(BASE_URL).resolve("/house");
    String requestBody = """
        {
          "floorCount": 2,
          "price": 10000,
          "parkingPlaces": [{
            "isCovered": true,
            "isWarm": false,
            "placesCount": 30
          }]
        }
        """;
    BodyPublisher bodyPublisher = BodyPublishers.ofString(requestBody, StandardCharsets.UTF_8);
    BodyHandler<String> bodyHandler = BodyHandlers.ofString(StandardCharsets.UTF_8);
    HttpRequest request = HttpRequest.newBuilder()
        .uri(uri)
        .header("Content-Type", "application/json")
        .header("Authorization", "Bearer " + authToken)
        .POST(bodyPublisher)
        .build();
    HttpClient client = HttpClient.newHttpClient();
    HttpResponse<String> response = client.send(request, bodyHandler);
    System.out.printf("Response code: %d\nBody: %s\n", response.statusCode(), response.body());
    String id = response.body().split(":")[1].split(",")[0];
    return id;
  }

  public static void settleUser(Authorization.AuthResponse authorization, User user, int houseId)
      throws IOException, InterruptedException {
    String strUri = String.format("/house/%d/settle/%d", houseId, user.id);
    URI uri = URI.create(BASE_URL).resolve(strUri);
    BodyPublisher bodyPublisher = BodyPublishers.noBody();
    BodyHandler<String> bodyHandler = BodyHandlers.ofString(StandardCharsets.UTF_8);
    HttpRequest request = HttpRequest.newBuilder()
        .uri(uri)
//        .header("Content-Type", "application/json")
        .header("Authorization", "Bearer " + authorization.accessToken)
        .POST(bodyPublisher)
        .build();
    HttpClient client = HttpClient.newHttpClient();
    HttpResponse<String> response = client.send(request, bodyHandler);
    System.out.printf("Response code: %d\nBody: %s\n", response.statusCode(), response.body());
  }
}
