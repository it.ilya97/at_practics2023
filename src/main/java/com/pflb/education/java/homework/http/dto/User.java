package com.pflb.education.java.homework.http.dto;

import java.util.Objects;

public class User {
  public int id;
  public String firstName;
  public String secondName;
  public double money;
  public int age;
  public Sex sex;

  public enum Sex {
    MALE, FEMALE
  }

  public User(int id, String firstName, String secondName, double money, int age, Sex sex) {
    this.id = id;
    this.firstName = firstName;
    this.secondName = secondName;
    this.money = money;
    this.age = age;
    this.sex = sex;
  }

  public User() {
  }

  public int getId() {
    return id;
  }

  @Override
  public String toString() {
    return "User{" +
            "id=" + id +
            ", firstName='" + firstName + '\'' +
            ", secondName='" + secondName + '\'' +
            ", money=" + money +
            ", age=" + age +
            ", sex=" + sex +
            '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    User user = (User) o;
    return id == user.id
            && Double.compare(user.money, money) == 0
            && age == user.age
            && firstName.equals(user.firstName)
            && secondName.equals(user.secondName)
            && sex == user.sex;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, firstName, secondName, money, age, sex);
  }
}
