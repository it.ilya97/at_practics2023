package com.pflb.education.java.homework.http.dto;

import java.util.Objects;

public class Car {
    public int id;
    public String mark;
    public EngineType engineType;
    public String model;
    public double price;

    public enum EngineType{
        Diesel,
        CNG,
        Hydrogenic,
        Electric,
        PHEV,
        Gasoline
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", mark='" + mark + '\'' +
                ", engineType=" + engineType +
                ", model='" + model + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return id == car.id && Double.compare(car.price, price) == 0 && mark.equals(car.mark) && engineType == car.engineType && model.equals(car.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, mark, engineType, model, price);
    }
}
