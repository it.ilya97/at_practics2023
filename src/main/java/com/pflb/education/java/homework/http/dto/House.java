package com.pflb.education.java.homework.http.dto;

import java.util.List;

public class House {
  public int id;
  public int floorCount;
  public double price;
  public List<ParkingPlace> parkingPlaces;

  public static class ParkingPlace {
    public int id;
    public boolean isCovered;
    public boolean isWarm;
    public int placesCount;
  }
}
